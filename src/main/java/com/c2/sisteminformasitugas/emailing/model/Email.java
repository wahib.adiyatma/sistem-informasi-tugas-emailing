package com.c2.sisteminformasitugas.emailing.model;

public class Email {
    public String[] toEmail;
    public String body;
    public String subject;

    public Email(String[] toEmail, String body, String subject) {
        this.toEmail = toEmail;
        this.body = body;
        this.subject = subject;
    }
}
