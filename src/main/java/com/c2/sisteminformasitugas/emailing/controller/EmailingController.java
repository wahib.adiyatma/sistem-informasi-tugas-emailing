package com.c2.sisteminformasitugas.emailing.controller;
import com.c2.sisteminformasitugas.emailing.model.Email;
import com.c2.sisteminformasitugas.emailing.service.EmailSenderService;
import com.c2.sisteminformasitugas.emailing.service.EmailSenderServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/email")
public class EmailingController {
    @Autowired
    EmailSenderService emailSenderService;

    @PostMapping(produces = {"application/json"})
    @ResponseBody
    public ResponseEntity postEmail(@RequestBody Email email) {
        emailSenderService.sendSimpleEmail(email.toEmail, email.body, email.subject);
        return ResponseEntity.ok(email);
    }
}
